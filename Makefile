.DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


.PHONY: install-local uninstall install deps tests dist check upload-test install-test upload clean


install-local: ## Install secenv as a local package
	python -m pip install -e .

uninstall: ## Uninstall secenv
	python -m pip uninstall secenv

install: ## Install secenv from PyPI store
	python -m pip install secenv

deps: ## Install test & build dependencies
	python -m pip install build twine pytest

tests: ## Run the unit tests
	python -m pytest -ra

dist: ## Generate wheel package for PyPI
	git stash
	python -m build
	git stash pop

check: ## Check package with twine
	python -m twine check dist/*

upload-test: ## Upload the package to TestPyPI
	python -m twine upload -r testpypi dist/*
	@echo "visit 'https://test.pypi.org/project/secenv/' to ensure everything is working"

install-test: ## Install secenv from TestPyPI
	python -m pip install -i https://test.pypi.org/simple secenv

upload: ## Upload the package to PyPI
	python -m twine upload dist/*
	@echo "visit 'https://pypi.org/project/secenv/'"

clean: ## Remove build and dist files
	-rm -r dist
	-rm -r secenv.egg-info
	-find . -type f -name '*.py[co]' -delete -o -type d -name "__pycache__" -delete
