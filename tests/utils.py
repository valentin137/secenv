import os
import pytest
import tempfile
import yaml
from contextlib import contextmanager


skip_unimplemented_test = pytest.mark.skip(reason="test not yet implemented")

skip_unimplemented_functionality = pytest.mark.skip(
    reason="functionality not yet implemented"
)


@contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


def with_tmp_dir(f):
    def inner():
        with tempfile.TemporaryDirectory() as tmpdir, cwd(tmpdir):
            f()

    return inner


def with_tmp_conf(f):
    def inner():
        with tempfile.TemporaryDirectory() as tmpdir, cwd(tmpdir):
            with open(".secenv.yml", "w") as config:
                f(config)

    return inner


def write(config_file, config_content):
    yaml.dump(config_content, config_file, default_flow_style=False)
    config_file.flush()
